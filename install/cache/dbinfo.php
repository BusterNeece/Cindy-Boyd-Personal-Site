[Database]

; Database settings go here. Driver must be a valid PDO driver.

<?php if ($data->driver == 'sqlite') { ?>
driver = sqlite
file = "conf/site.db"
<?php } else { ?>
driver = <?php echo Template::sanitize ($data->driver, 'UTF-8'); ?>

host = "<?php echo Template::sanitize ($data->host, 'UTF-8'); ?>:<?php echo Template::sanitize ($data->port, 'UTF-8'); ?>"

name = <?php echo Template::sanitize ($data->name, 'UTF-8'); ?>

user = <?php echo Template::sanitize ($data->user, 'UTF-8'); ?>

pass = "<?php echo Template::sanitize ($data->pass, 'UTF-8'); ?>"
<?php } ?>

[Hooks]