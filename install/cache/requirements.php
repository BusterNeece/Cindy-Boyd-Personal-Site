<!DOCTYPE html>
<html>
<head>
	<title>Elefant Installer</title>
	<link rel="stylesheet" type="text/css" href="css/style.css" />
</head>
<body>
<div id="wrapper">
	<h1><span>Elefant</span> Installer</h1>
	
	<div id="steps">
		<ul>
			<li>Introduction</li>
			<li>License</li>
			<li class="active">Requirements</li>
			<li>Database</li>
			<li>Settings</li>
			<li>Finished</li>
		</ul>
	</div>

	<div id="body"><div id="content">

<h3>Testing Requirements</h3>

<?php if ($data->passed) { ?>
<p>Server requirements have been verified.</p>

<p>Click "Next" to continue.</p>
<?php } else { ?>
<p>One or more server requirements failed:</p>

<ul>
<?php foreach ($data->req as $data->loop_index => $data->loop_value) { ?>
	<?php if ($data->loop_value < 1) { ?>
	<li><?php echo $data->loop_index; ?></li>
	<?php } ?>
<?php } ?>
</ul>

<p>Please correct the problem and try again.</p>
<?php } ?>

	</div></div>
	<?php if ($data->passed) { ?>
	<a class="next" href="/install/?step=database">Next: Database</a>
	<?php } else { ?>
	<a class="next" href="/install/?step=requirements">Try Again</a>
	<?php } ?>
</div>
</body>
</html>
