<!DOCTYPE html>
<html>
<head>
	<title>Elefant Installer</title>
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<script>
		function verify_password (f) {
			if (f.elements.pass.value != f.elements.verify.value) {
				document.getElementById ('vnotice').style.display = 'inline';
				return false;
			}
			document.getElementById ('vnotice').style.display = 'none';
			return true;
		}
	</script>
</head>
<body>
<div id="wrapper">
	<h1><span>Elefant</span> Installer</h1>
	
	<div id="steps">
		<ul>
			<li>Introduction</li>
			<li>License</li>
			<li>Requirements</li>
			<li>Database</li>
			<li class="active">Settings</li>
			<li>Finished</li>
		</ul>
	</div>

	<div id="body"><div id="content">

<?php if ($data->error) { ?>
<h3>Error:</h3>
<p class="notice"><?php echo Template::sanitize ($data->error, 'UTF-8'); ?></p>
<?php } else { ?>
<h3>Site Settings</h3>
<?php } ?>

<?php if ($data->ready) { ?>
<p>Settings saved and admin user created.</p>

<p>Click "Next" to continue.</p>
<?php } else { ?>
<form method="POST" action="/install/?step=settings" onsubmit="return verify_password (this)">

<p>Site Name:<br />
<input type="text" name="site_name" value="<?php echo Template::sanitize ($_POST['site_name'], 'UTF-8'); ?>" size="30" /></p>
<p>Your Name:<br />
<input type="text" name="your_name" value="<?php echo Template::sanitize ($_POST['your_name'], 'UTF-8'); ?>" size="30" /></p>
<p>Admin Email:<br />
<input type="text" name="email_from" value="<?php echo Template::sanitize ($_POST['email_from'], 'UTF-8'); ?>" size="30" /></p>
<p>Admin Password:<br />
<input type="password" name="pass" value="<?php echo Template::sanitize ($_POST['pass'], 'UTF-8'); ?>" size="20" /></p>
<p>Verify Password:<br />
<input type="password" name="verify" value="<?php echo Template::sanitize ($_POST['verify'], 'UTF-8'); ?>" size="20" onblur="verify_password (this.form)" />
<span class="notice" id="vnotice" style="display: none">Password does not match.</span></p>

<p><input type="submit" value="Save Settings" /></p>
</form>
<?php } ?>

	</div></div>
	<?php if ($data->ready) { ?>
	<a class="next" href="/install/?step=finished">Next: Finished</a>
	<?php } ?>
</div>
</body>
</html>
