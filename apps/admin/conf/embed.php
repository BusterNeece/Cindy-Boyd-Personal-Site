; <?php /*

[admin/forward]

label = "Pages: Redirect Link"

to[label] = Link
to[type] = text
to[initial] = "http://"
to[not empty] = 1
to[regex] = "|^http://.+$|"
to[message] = Please enter a valid URL.

[admin/menu]

label = "Pages: Menu Links"

; */ ?>